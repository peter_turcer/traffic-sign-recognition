package com.example.rozpoznavanieznaciek.env;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Pomocná trieda slúžiaca na manipuláciu so obrázkami.
 */
public class ImageUtils {

    static final int kMaxChannelValue = 262143;

    /**
     * Pomocná metóda, ktorá vypočíta alokovanú
     * veľkosť v bytoch obrázku typu YUV420SP zadanej veľkosti.
     *
     * @param width Šírka obrázka
     * @param height Výška obrázka
     */
    public static int getYUVByteSize(final int width, final int height) {
        final int ySize = width * height;
        final int uvSize = ((width + 1) / 2) * ((height + 1) / 2) * 2;

        return ySize + uvSize;
    }

  /**
   * Pomocná metóda, ktorá slúži na konverziu bytového poľa uloženého v
   * databáze, na formu obrázka, ktorý je možné vykresliť v
   * grafickom rozhraní mobilnej aplikácie.
   *
   * @param image Obrázok vo formáte bytového poľa z databázy
   */
    public static Drawable ByteArrayToDrawable(byte[] image) {
        Drawable resultImage = new BitmapDrawable(BitmapFactory.decodeByteArray(image, 0, image.length));
        return resultImage;
    }

  /**
   * Metóda konvertuje obrázok uložení priamo v aplikácií n
   * a tvar potrebný pre uloženie do databázy.
   *
   * @param image Obrázok vo formáte Drawable
   */
    public static byte[] DrawableToByteArray(Drawable image) {
        Bitmap bitmap = ((BitmapDrawable) image).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bitmapdata = stream.toByteArray();
        return bitmapdata;
    }


    public static void convertYUV420SPToARGB8888(byte[] input, int width, int height, int[] output) {
        final int frameSize = width * height;
        for (int j = 0, yp = 0; j < height; j++) {
            int uvp = frameSize + (j >> 1) * width;
            int u = 0;
            int v = 0;

            for (int i = 0; i < width; i++, yp++) {
                int y = 0xff & input[yp];
                if ((i & 1) == 0) {
                    v = 0xff & input[uvp++];
                    u = 0xff & input[uvp++];
                }

                output[yp] = YUV2RGB(y, u, v);
            }
        }
    }

    private static int YUV2RGB(int y, int u, int v) {
        y = (y - 16) < 0 ? 0 : (y - 16);
        u -= 128;
        v -= 128;

        // nR = (int)(1.164 * nY + 2.018 * nU);
        // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
        // nB = (int)(1.164 * nY + 1.596 * nV);
        int y1192 = 1192 * y;
        int r = (y1192 + 1634 * v);
        int g = (y1192 - 833 * v - 400 * u);
        int b = (y1192 + 2066 * u);

        r = r > kMaxChannelValue ? kMaxChannelValue : (r < 0 ? 0 : r);
        g = g > kMaxChannelValue ? kMaxChannelValue : (g < 0 ? 0 : g);
        b = b > kMaxChannelValue ? kMaxChannelValue : (b < 0 ? 0 : b);

        return 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
    }

    /**
     * Vracia transformačnú maticu z jedného referenčného pohľadu na druhý.
     * Zvláda aj orezávanie alebo rotáciu ak je to potrebné.
     *
     * @param srcWidth            Šírka zdrojového rámca.
     * @param srcHeight           Výška zdrojového rámca.
     * @param dstWidth            Šírka cieľového rámca.
     * @param dstHeight           Výška cieľového rámca.
     * @param applyRotation       Hodnota rotácia aplikovaná z jedného rámca na druhý.
     * @param maintainAspectRatio Aa je hodnota true, zabezpečí, že mierka x a y zostane konštantná.
     *
     * @return Transformácia napĺňajúca požadované vlastnosti.
     */
    public static Matrix getTransformationMatrix(
            final int srcWidth,
            final int srcHeight,
            final int dstWidth,
            final int dstHeight,
            final int applyRotation,
            final boolean maintainAspectRatio) {
        final Matrix matrix = new Matrix();

        if (applyRotation != 0) {

            // Translate so center of image is at origin.
            matrix.postTranslate(-srcWidth / 2.0f, -srcHeight / 2.0f);

            // Rotate around origin.
            matrix.postRotate(applyRotation);
        }

        // Account for the already applied rotation, if any, and then determine how
        // much scaling is needed for each axis.
        final boolean transpose = (Math.abs(applyRotation) + 90) % 180 == 0;

        final int inWidth = transpose ? srcHeight : srcWidth;
        final int inHeight = transpose ? srcWidth : srcHeight;

        // Apply scaling if necessary.
        if (inWidth != dstWidth || inHeight != dstHeight) {
            final float scaleFactorX = dstWidth / (float) inWidth;
            final float scaleFactorY = dstHeight / (float) inHeight;

            if (maintainAspectRatio) {
                // Scale by minimum factor so that dst is filled completely while
                // maintaining the aspect ratio. Some image may fall off the edge.
                final float scaleFactor = Math.max(scaleFactorX, scaleFactorY);
                matrix.postScale(scaleFactor, scaleFactor);
            } else {
                // Scale exactly to fill dst from src.
                matrix.postScale(scaleFactorX, scaleFactorY);
            }
        }

        if (applyRotation != 0) {
            // Translate back from origin centered reference to destination frame.
            matrix.postTranslate(dstWidth / 2.0f, dstHeight / 2.0f);
        }

        return matrix;
    }
}
