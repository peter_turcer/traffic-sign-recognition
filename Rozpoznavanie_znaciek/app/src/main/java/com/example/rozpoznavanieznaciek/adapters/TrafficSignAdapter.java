package com.example.rozpoznavanieznaciek.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rozpoznavanieznaciek.R;
import com.example.rozpoznavanieznaciek.TrafficSignHolder;

import java.util.ArrayList;

public class TrafficSignAdapter extends RecyclerView.Adapter<TrafficSignAdapter.TrafficSignViewHolder> {

    private  ArrayList<TrafficSignHolder> trafficSignList;

    public static class TrafficSignViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textHolder;

        public TrafficSignViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.signListImage);
            textHolder = itemView.findViewById(R.id.signListTextHolder);
        }
    }

    public TrafficSignAdapter(ArrayList<TrafficSignHolder> trafficSignList)
    {
        this.trafficSignList = trafficSignList;
    }


    @NonNull
    @Override
    public TrafficSignViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.traffic_sign_card_resource, viewGroup, false);
        TrafficSignViewHolder evh = new TrafficSignViewHolder(v);
        return evh;
    }

    /**
     * Slúži na previazanie a načítanie celého zoznamu inštancií alebo
     * inač povedané kariet, ktoré sa načítajú do komponentu RecyclerView,
     *
     * @param holder
     * @param i Poradie načítaného objektu.
     */
    @Override
    public void onBindViewHolder(@NonNull TrafficSignViewHolder holder, int i) {
        TrafficSignHolder trafficSignHolder = trafficSignList.get(i);

        holder.textHolder.setTextSize(TypedValue.COMPLEX_UNIT_DIP, trafficSignHolder.getTextSize());

        holder.imageView.setImageDrawable(trafficSignHolder.getImageResource());
        if(trafficSignHolder.getOpened() == 0)
             {holder.textHolder.setText(Html.fromHtml("<b>"+ trafficSignHolder.getTextHolder()+"</b>")); }
        else
             {
                 holder.textHolder.setText(Html.fromHtml("<b>"+ trafficSignHolder.getName()+"</b>" + "<br>" + trafficSignHolder.getTextHolder()));
             }
    }

    @Override
    public int getItemCount() {
        return trafficSignList.size();
    }
}
