package com.example.rozpoznavanieznaciek.activities;

import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.rozpoznavanieznaciek.R;
import com.example.rozpoznavanieznaciek.DatabaseHelper;
import com.example.rozpoznavanieznaciek.adapters.TrafficSignAdapter;
import com.example.rozpoznavanieznaciek.TrafficSignHolder;
import com.example.rozpoznavanieznaciek.env.ImageUtils;

import java.util.ArrayList;

public class TrafficSignListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<TrafficSignHolder> trafficSignList;

    private DatabaseHelper db;
    private static final String TABLE_NAME_SIGNS = "TrafficSigns";

    private TextView textNote, signProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic_sign_list);
        db = new DatabaseHelper(this);
        trafficSignList = new ArrayList<>();

        ImageButton paragraphButton = findViewById(R.id.buttonParagraph);
        ImageButton infoButton = findViewById(R.id.buttonInfo);

        Toolbar toolbar = findViewById(R.id.headerToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new TrafficSignAdapter(trafficSignList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


        textNote = findViewById(R.id.layoutListText);
        signProgress = findViewById(R.id.singProgress);


        loadTrafficSingCardsFromDB();
        signProgress.setText(db.sumOpenTrafficSigns() + "/"+ db.getTableSize("TrafficSigns"));

        paragraphButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeToPrimaryTexts();
                textNote.setText("Znenie zákona dopravných značiek");
                adapter.notifyDataSetChanged();
            }
        });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeToInstructorTexts();
                textNote.setText("Výklad inštruktora");
                adapter.notifyDataSetChanged();
            }
        });
    }


    /**
     * Podporná metóda, ktorá slúži na zmenu všetkých textových polí
     * na primárny text, využívaná pri stlačení tlačidla používateľom aplikácie,
     */
    public void changeToPrimaryTexts()
    {
        for (int i = 0; i < trafficSignList.size(); i++) {
                CharSequence text = trafficSignList.get(i).getPrimaryText();
                trafficSignList.get(i).setTextHolder(text);
            }
    }

    public void changeToInstructorTexts()
    {
        for (int i = 0; i < trafficSignList.size(); i++) {
            CharSequence text = trafficSignList.get(i).getSecondaryText();
            trafficSignList.get(i).setTextHolder(text);
        }

    }

    /**
     * Metóda zabezpečuje načítanie všetkých dopravných značiek a ich
     * súčastí (textu, obrázkov) z databázy. Následne ich vloží do objektu
     * typu ArrayList, ktorý spracuje iná trieda pri výpise dopravných značiek.
     */
    public void loadTrafficSingCardsFromDB()
    {
        for (int i = 1; i < db.getTableSize(TABLE_NAME_SIGNS)+1; i++) {

            Cursor cursor = db.getColumn(i);
            float textSize = getResources().getDimension(R.dimen.openTextSize) / getResources().getDisplayMetrics().density;

            if (cursor.moveToFirst())
            {
                String name = cursor.getString(cursor.getColumnIndex("NAME"));
                String primaryText = cursor.getString(cursor.getColumnIndex("TEXT_PRIMARY"));
                String textHolder = primaryText;
                String secondaryText = cursor.getString(cursor.getColumnIndex("TEXT_INSTRUCTOR"));
                Long opened = cursor.getLong(cursor.getColumnIndex("OPEN_STATUS"));
                byte[] image = cursor.getBlob(cursor.getColumnIndex("IMAGE"));

                Drawable drawImage = ImageUtils.ByteArrayToDrawable(image);

                if (opened == 0) {
                    primaryText = "ZNAČKA ZATVORENÁ";
                    secondaryText = "ZNAČKA ZATVORENÁ";
                    textHolder = "ZNAČKA ZATVORENÁ";
                    name = "";
                    textSize = getResources().getDimension(R.dimen.closedTextSize) / getResources().getDisplayMetrics().density ;
                }

                trafficSignList.add(new TrafficSignHolder(drawImage, textHolder,  primaryText, secondaryText, name, opened, textSize));
            }
            cursor.close();
        }
    }

}