package com.example.rozpoznavanieznaciek;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class ChallengeManager {

    private static final String SHARED_PREFERENCES_FILE = "prefs";
    private static final String CHALLENGE_KEY = "trafficSign_challenge";

    private static ChallengeManager challengeManager;
    private Challenge challenge;

    /**
     * Metóda zastrešujúca využitie návrhového vzoru Singleton.
     * Vytvorí len jeden objekt a ak je tento objekt pri opätovnom
     * volaní tejto metódy už vytvorený tak pôvodný objekt vráti.
     *
     * @param context Globálna informácia o aplikačnom prostredí
     */
    public static ChallengeManager getInstance(Context context) {
        if (challengeManager == null) {
            challengeManager = new ChallengeManager(context);
        }
        return challengeManager;
    }

    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    private ChallengeManager(Context context) {
        this.context = context.getApplicationContext();
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * Metóda využíva návrhový vzor Singleton.
     * Ak ešte objekt typu Challenge neexistuje tak ho vytvorí a vráti ho.
     * Ak už objekt Challenge existuje tak vráti daný objekt do ktorého
     * vloží aktuálne dáta, ktoré sú serializované v SharredPreferences.
     */
    public Challenge getChallenge() {

        String serializedChallenge = sharedPreferences.getString(CHALLENGE_KEY, null);

        if (serializedChallenge == null) {
            challenge = new Challenge();
        } else {
            serializedChallenge = sharedPreferences.getString(CHALLENGE_KEY, null);
            try {
                challenge = (Challenge) deserialize(serializedChallenge);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return challenge;
    }

    /**
     * Do parametra je vložený aktuálny objekt typu Challenge
     * spravovaný a upravený aplikáciou. Tento objekt sa následne
     * serializuje a uloží do SharredPreferences.
     *
     * @param challenge Objekt obsahujúci všetky dáta o výzve
     */
    public void updateChallenge(Challenge challenge) {
        String serializedChallenge = null;
        try {
            serializedChallenge = serialize(challenge);
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.putString(CHALLENGE_KEY, serializedChallenge);
        editor.apply();
    }

    public static Object deserialize(String s) throws IOException, ClassNotFoundException {
        byte[] bytes = Base64.decode(s.getBytes(), Base64.DEFAULT);
        ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
        return objectInputStream.readObject();
    }

    public static String serialize(Serializable o) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(o);
        objectOutputStream.close();
        return new String(Base64.encode(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
    }

}
