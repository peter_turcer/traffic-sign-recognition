package com.example.rozpoznavanieznaciek;

import android.graphics.drawable.Drawable;

public class TrafficSignHolder {
    private Drawable imageResource;
    private CharSequence textHolder;
    private CharSequence primaryText;
    private CharSequence secondaryText;
    private long opened;
    private float textSize;
    private String name;


    public TrafficSignHolder(Drawable imageResource, CharSequence textHolder, CharSequence primaryText, CharSequence secondaryText, String name, long opened, float textSize) {
        this.imageResource = imageResource;
        this.textHolder = textHolder;
        this.primaryText = primaryText;
        this.secondaryText = secondaryText;
        this.opened = opened;
        this.textSize = textSize;
        this.name = name;
    }

    public Drawable getImageResource() {
        return imageResource;
    }

    public CharSequence getTextHolder() {
        return textHolder;
    }

    public void setTextHolder(CharSequence textHolder) {
        this.textHolder = textHolder;
    }

    public CharSequence getPrimaryText() {
        return primaryText;
    }

    public CharSequence getSecondaryText() {
        return secondaryText;
    }

    public long getOpened() {
        return opened;
    }

    public float getTextSize() {
        return textSize;
    }

    public String getName() {
        return name;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public void setName(String name) {
        this.name = name;
    }
}
