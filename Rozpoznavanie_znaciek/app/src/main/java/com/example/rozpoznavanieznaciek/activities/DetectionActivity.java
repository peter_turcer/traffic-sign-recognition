package com.example.rozpoznavanieznaciek.activities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.Size;
import android.util.TypedValue;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.rozpoznavanieznaciek.view.BoxTracker;
import com.example.rozpoznavanieznaciek.Classifier;
import com.example.rozpoznavanieznaciek.R;
import com.example.rozpoznavanieznaciek.TFLiteModel;
import com.example.rozpoznavanieznaciek.env.BorderedText;
import com.example.rozpoznavanieznaciek.env.ImageUtils;
import com.example.rozpoznavanieznaciek.view.OverlayView;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class DetectionActivity extends CameraActivity {

    private static final int TF_OD_API_INPUT_SIZE = 300;
    private static final String TF_OD_API_MODEL_FILE = "slovenskeZnacky.tflite";
    private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/skZnackyLabelMap.txt";
    private static final Size DESIRED_PREVIEW_SIZE = new Size(1280, 960);

    //private static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.7f;
    private static float MINIMUM_CONFIDENCE_TF_OD_API = 0.7f;

    private static final boolean MAINTAIN_ASPECT = false;

    private Classifier detector;
    private BoxTracker tracker;

    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;

    protected int previewWidth = 0;
    protected int previewHeight = 0;

    private boolean computingDetection = false;
    private OverlayView trackingOverlay;

    private Integer sensorOrientation;

    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;
    private Bitmap cropCopyBitmap = null;

    private String recognitionLabel = "";

    private float left;
    private float top;
    private float right;
    private float bottom;

    /**
     * Metóda, ktorá inicializuje triedy ako napríklad BoxTracker.
     * Vytvára bitmapy alebo registruje callback slúžiaci na vykresľovanie na plátno.
     *
     * @param size     Určuje veľkosť preview
     * @param rotation Zabezpečuje orientáciou senzora
     */
    @Override
    public void onPreviewSizeChosen(final Size size, final int rotation) {

        tracker = new BoxTracker(this);

        int cropSize = TF_OD_API_INPUT_SIZE;

        try {
            detector = TFLiteModel.create(getAssets(), TF_OD_API_MODEL_FILE, TF_OD_API_LABELS_FILE, TF_OD_API_INPUT_SIZE);
            cropSize = TF_OD_API_INPUT_SIZE;
        } catch (final IOException e) {
            e.printStackTrace();
            Toast toast =
                    Toast.makeText(
                            getApplicationContext(), "Classifier could not be initialized", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        }

        previewWidth = size.getWidth();
        previewHeight = size.getHeight();

        sensorOrientation = rotation - getScreenOrientation();

        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.ARGB_8888);
        croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Bitmap.Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(previewWidth, previewHeight, cropSize, cropSize, sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);

        trackingOverlay = (OverlayView) findViewById(R.id.overlayView);

        trackingOverlay.addCallback(
                new OverlayView.DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        tracker.draw(canvas);

                        left = tracker.getLeft();
                        right = tracker.getRight();
                        top = tracker.getTop();
                        bottom = tracker.getBottom();
                    }
                });

        tracker.setFrameConfiguration(previewWidth, previewHeight, sensorOrientation);
    }

    /**
     * Metóda vo vlákne spracováva List rozpoznaných objektov,
     * rozdeľuje ich na základe pravdepodobnosti a ďalej posiela triede
     * BoxTracker na spracovanie
     */
    @Override
    protected void processImage() {
        trackingOverlay.postInvalidate();

        if (computingDetection) {
            readyForNextImage();
            return;
        }

        computingDetection = true;
        rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

        readyForNextImage();

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);

        runInBackground(
                new Runnable() {
                    @Override
                    public void run() {

                        final List<Classifier.Recognition> results = detector.recognizeImage(croppedBitmap);

                        cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);
                        final Canvas canvas = new Canvas(cropCopyBitmap);
                        final Paint paint = new Paint();
                        paint.setColor(Color.RED);
                        paint.setStyle(Paint.Style.STROKE);
                        paint.setStrokeWidth(2.0f);

                        float minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;

                        final List<Classifier.Recognition> mappedRecognitions =
                                new LinkedList<Classifier.Recognition>();

                        recognitionLabel = "";

                        for (final Classifier.Recognition result : results) {
                            final RectF location = result.getLocation();
                            if (location != null && result.getConfidence() >= minimumConfidence) {
                                canvas.drawRect(location, paint);
                                recognitionLabel = result.getTitle();
                                cropToFrameTransform.mapRect(location);

                                result.setLocation(location);
                                mappedRecognitions.add(result);
                            }
                        }

                        tracker.trackResults(mappedRecognitions);

                        trackingOverlay.postInvalidate();
                        computingDetection = false;
                    }
                });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_camera_fragment;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
    }

    @Override
    protected String getLabelName() {
        return recognitionLabel;
    }

    @Override
    protected float getLeft() {
        return left;
    }

    @Override
    protected float getRight() {
        return right;
    }

    @Override
    protected float getBottom() {
        return bottom;
    }

    @Override
    protected float getTop() {
        return top;
    }

    @Override
    protected float changeConfidence() {
        MINIMUM_CONFIDENCE_TF_OD_API = MINIMUM_CONFIDENCE_TF_OD_API + 0.1f;
        MINIMUM_CONFIDENCE_TF_OD_API = round(MINIMUM_CONFIDENCE_TF_OD_API, 2);

        if (MINIMUM_CONFIDENCE_TF_OD_API > 0.9f) {
            MINIMUM_CONFIDENCE_TF_OD_API = 0.7f;
        }

        return MINIMUM_CONFIDENCE_TF_OD_API;
    }

    public float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

}