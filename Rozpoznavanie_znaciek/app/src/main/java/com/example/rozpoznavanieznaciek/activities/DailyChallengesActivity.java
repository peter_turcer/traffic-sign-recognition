package com.example.rozpoznavanieznaciek.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import com.example.rozpoznavanieznaciek.Challenge;
import com.example.rozpoznavanieznaciek.ChallengeManager;
import com.example.rozpoznavanieznaciek.R;
import com.example.rozpoznavanieznaciek.DatabaseHelper;
import com.example.rozpoznavanieznaciek.env.ImageUtils;

import java.util.Locale;
import java.util.Random;

public class DailyChallengesActivity extends AppCompatActivity {

    private ImageButton statisticsButton;
    private ImageButton stopButton;
    private Button startButton;
    private Switch switchDiff;

    private ImageView imgCheck1;
    private ImageView imgCheck2;
    private ImageView imgCheck3;

    private TextView timer;

    private static Random genSeed;
    private static Random genSigns;
    private static Random genDiff;
    public static Random genSum;

    private DatabaseHelper db;
    private static final String TABLE_NAME_SIGNS = "TrafficSigns";

    private static final long START_TIME_IN_MILLIS_EASY = 28800000; //1000 * 60 * 60 * 8
    private String hourFormatEasy = "08:00:00";
    private static final long START_TIME_IN_MILLIS_HARD = 86400000; //86400000; //1000 * 60 * 60 * 24‬
    private String hourFormatHard = "24:00:00";

    private CountDownTimer countDownTimer;

    private int dbSignSize;

    private Challenge challenge;
    private ChallengeManager challengeManager;

    /**
     * Metóda slúži na načítanie všetkých pohľadov, ktoré
     * sú použití na inicializáciu generátorov náhodných
     * čísel a nastavenie listenerov pre používanie tlačidiel.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_quests);

        genSeed = new Random();
        genSigns = new Random(genSeed.nextLong());
        genDiff = new Random(genSeed.nextLong());
        genSum = new Random(genSeed.nextLong());

        db = new DatabaseHelper(this);
        dbSignSize = db.getTableSize(TABLE_NAME_SIGNS);

        challengeManager = challengeManager.getInstance(this);
        challenge = challengeManager.getChallenge();

        setViews();
        //insertDataToStatDB(10);

        statisticsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showPopupWindow(findViewById(R.id.dailyQuestMainLayout), R.layout.activity_popup_statistics);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                resetChallenge(false);
                updateChallenge();
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (switchDiff.isChecked()) {

                    challenge.setTimeLeftInMillis(START_TIME_IN_MILLIS_HARD);
                    resetImageViews();
                    startTimer();


                    int[] ex = {0};
                    int[] ex2 = {0, 0};

                    int number1 = genSigns.nextInt(dbSignSize) + 1;
                    challenge.setSign1(number1);

                    ex[0] = number1;
                    int number2 = getRandomWithExclusion(genSeed, 1, dbSignSize, ex);
                    challenge.setSign2(number2);

                    ex2[0] = number1;
                    ex2[1] = number2;

                    int number3 = getRandomWithExclusion(genSeed, 1, dbSignSize, ex2);
                    challenge.setSign3(number3);

                    Cursor cursor1 = db.getColumn(number1);
                    Cursor cursor2 = db.getColumn(number2);
                    Cursor cursor3 = db.getColumn(number3);

                    populateChallengeSignField(cursor1, R.id.imageChallenge1, R.id.textViewChallenge1);
                    populateChallengeSignField(cursor2, R.id.imageChallenge2, R.id.textViewChallenge2);
                    populateChallengeSignField(cursor3, R.id.imageChallenge3, R.id.textViewChallenge3);
                } else {
                    challenge.setTimeLeftInMillis(START_TIME_IN_MILLIS_EASY);
                    resetImageViews();
                    startTimer();

                    int rand = genSigns.nextInt(dbSignSize) + 1;
                    challenge.setSign1(rand);
                    Cursor cursor1 = db.getColumn(rand);

                    populateChallengeSignField(cursor1, R.id.imageChallenge1, R.id.textViewChallenge1);
                }
            }
        });

        switchDiff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    switchDiff.setText("Ťažká výzva");
                    challenge.setSwitchChecked(true);
                    if (!challenge.isTimerRunning()) {
                        timer.setText(hourFormatHard);
                        resetImageViews();
                    }
                } else {
                    switchDiff.setText("Ľahká výzva");
                    challenge.setSwitchChecked(false);
                    if (!challenge.isTimerRunning()) {
                        timer.setText(hourFormatEasy);
                        resetImageViews();
                    }
                }
            }
        });
    }

    /**
     * Táto časť životného cyklu aktivity zabezpečuje všetky možnosti, ktoré môžu
     * nastať pri spustení aktivity čo sa týka výziev ako aj možností keď
     * prechádzame do alebo prichádzame z inej aktivity.
     */
    @Override
    public void onStart() {
        super.onStart();

        challengeManager = challengeManager.getInstance(this);
        challenge = challengeManager.getChallenge();


        switchDiff.setChecked(challenge.isSwitchChecked());

        //updateCountdownTimer();
        if (challenge.isWantedSignDone1()) {
            imgCheck1.setImageResource(R.drawable.icon_check_sign);
        }
        if (challenge.isWantedSignDone2()) {
            imgCheck2.setImageResource(R.drawable.icon_check_sign);
        }
        if (challenge.isWantedSignDone3()) {
            imgCheck3.setImageResource(R.drawable.icon_check_sign);
        }

        populateChallengeSignFieldOnStart();

        if (challenge.wasChallengeRunning()) {

            //updateCountdownTimer();

            switchDiff.setEnabled(false);
            startButton.setEnabled(false);
            stopButton.setVisibility(View.VISIBLE);


            if (checkResultsDone()) {
                resetChallenge(true);
                updateChallenge();
            } else if (challenge.getTimeLeftInMillis() < 0) {
                resetChallenge(false);
                updateChallenge();
            } else {
                startTimer();
            }
        }
        updateCountdownTimer();
    }

    @Override
    public void onBackPressed() {
        updateChallenge();

        Intent intent = new Intent(this, DetectionActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onStop() {
        super.onStop();

        updateChallenge();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Intent intent = new Intent(this, DetectionActivity.class);

                updateChallenge();

                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Táto metóda zabezpečuje všetku prácu týkajúcu sa vyskakovacích okien.
     * Potrebné je zadať pohľad kde budeme dáta vypisovať a layout, ktorý tieto
     * dáta, napríklad textové polia obsahuje.
     *
     * @param view         Komponent, ktorý vykskovacie okno prekryje.
     * @param inflatedView Konkrétny komponent vyskakovacieho okna.
     */
    public void showPopupWindow(View view, int inflatedView) {

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(inflatedView, null);

        TextView twEasyDone = popupView.findViewById(R.id.statEasyDone);
        TextView twEasyFailed = popupView.findViewById(R.id.statEasyFail);
        TextView twHardDone = popupView.findViewById(R.id.statHardDone);
        TextView twHardFailed = popupView.findViewById(R.id.statHardFail);
        TextView twSumSigns = popupView.findViewById(R.id.statSignsOverall);

        TextView twTimeEasy = popupView.findViewById(R.id.statTimeEasy);
        TextView twTimeHard = popupView.findViewById(R.id.statTimeHrad);

        twEasyDone.setText(db.sumEasyDone() + "");
        twEasyFailed.setText(db.sumEasyFailed() + "");
        twHardDone.setText(db.sumHardDone() + "");
        twHardFailed.setText(db.sumHardFailed() + "");
        twSumSigns.setText(db.totalCollectedSigns() + "");

        twTimeEasy.setText(formatMilliseconds(db.averageChallengeTime(0)));
        twTimeHard.setText(formatMilliseconds(db.averageChallengeTime(1)));


        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }


    /**
     * Metóda zabezpečuje generovanie náhodných celých čísel
     * v intervale od start do end, ale vylúči čísla, ktoré
     * sa nachádzajú v parametri exclude.
     *
     * @param rnd     Random generátor..
     * @param start   Celé číslo, začiatok generovaného intervalu.
     * @param end     Celé číslo, koniec generovaného intervalo
     * @param exclude Celé čísla, ktoré vylučujeme z generovaného intervalu.
     */
    public int getRandomWithExclusion(Random rnd, int start, int end, int... exclude) {
        int random = start + rnd.nextInt(end - start + 1 - exclude.length);
        for (int ex : exclude) {
            if (random < ex) {
                break;
            }
            random++;
        }
        return random;
    }

    public void insertRandomDataToStatDB(int paNumChallenges) {
        db.recreateTableStat();

        for (int i = 0; i < paNumChallenges; i++) {

            int diff = genDiff.nextInt(2);
            int sum = 0;

            if (diff == 0) {
                sum = genSum.nextInt(2);
            } else {
                sum = genSum.nextInt(4);
            }

            db.insertStatData(diff, sum, 10L);
        }
    }

    public void insertDataToStatDB() {
        int diff = 0;
        int done = 0;
        long challengeDuration = 0;

        if (challenge.isSwitchChecked()) {
            diff = 1;
        }

        if (challenge.isWantedSignDone1()) {
            done++;
        }
        if (challenge.isWantedSignDone2()) {
            done++;
        }
        if (challenge.isWantedSignDone3()) {
            done++;
        }

        if(challenge.isSwitchChecked())
        {
            if(challenge.getTimeLeftInMillis() > START_TIME_IN_MILLIS_HARD)
            {challengeDuration = START_TIME_IN_MILLIS_HARD;}
            else
                {challengeDuration = START_TIME_IN_MILLIS_HARD - challenge.getTimeLeftInMillis();}

        }else
        {
            if(challenge.getTimeLeftInMillis() > START_TIME_IN_MILLIS_EASY)
            {challengeDuration = START_TIME_IN_MILLIS_EASY;}
            else
            {challengeDuration = START_TIME_IN_MILLIS_EASY - challenge.getTimeLeftInMillis();}
        }

        db.insertStatData(diff, done, challengeDuration);
    }

    public void resetImageViews() {
        ImageView imageHolder1 = findViewById((R.id.imageChallenge1));
        TextView textHolder1 = findViewById(R.id.textViewChallenge1);

        ImageView imageHolder2 = findViewById((R.id.imageChallenge2));
        TextView textHolder2 = findViewById(R.id.textViewChallenge2);

        ImageView imageHolder3 = findViewById((R.id.imageChallenge3));
        TextView textHolder3 = findViewById(R.id.textViewChallenge3);

        imgCheck1.setImageDrawable(null);
        imgCheck2.setImageDrawable(null);
        imgCheck3.setImageDrawable(null);

        imageHolder1.setImageDrawable(null);
        textHolder1.setText("");

        imageHolder2.setImageDrawable(null);
        textHolder2.setText("");

        imageHolder3.setImageDrawable(null);
        textHolder3.setText("");
    }


    /**
     * Metóda spravujúca časovač výziev. Pri spustení alebo ukončení
     * mení nastavenia niektorých komponentov grafického rozhrania.
     */
    public void startTimer() {

        countDownTimer = new CountDownTimer(challenge.getTimeLeftInMillis(), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                challenge.setTimeLeftInMillis(millisUntilFinished);
                updateCountdownTimer();
            }

            @Override
            public void onFinish() {
                resetChallenge(false);
                updateChallenge();

            }
        }.start();

        stopButton.setVisibility(View.VISIBLE);
        switchDiff.setEnabled(false);
        startButton.setEnabled(false);
        challenge.setTimerRunning(true);
        challenge.setWasChallengeRunning(true);

    }

    public void resetChallenge(boolean paChallengeDone) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        insertDataToStatDB();

        stopButton.setVisibility(View.INVISIBLE);
        switchDiff.setEnabled(true);
        startButton.setEnabled(true);

        if (!challenge.isSwitchChecked()) {
            challenge.setTimeLeftInMillis(START_TIME_IN_MILLIS_EASY);
        } else {
            challenge.setTimeLeftInMillis(START_TIME_IN_MILLIS_HARD);
        }

        if (paChallengeDone) {
            timer.setText("VÝZVA ÚSPEŠNÁ");
        } else {
            timer.setText("VÝZVA NEÚSPEŠNÁ");
            checkResultsFailed();
        }

        challenge.resetAll();
    }

    /**
     * Metóda slúži na formátovanie prebiehajúceho času v
     * milisekundách, nastavuje čas vo formáte hodiny:minúty:sekúndy.
     */
    public void updateCountdownTimer() {
        int seconds = (int) (challenge.getTimeLeftInMillis() / 1000) % 60;
        int minutes = (int) ((challenge.getTimeLeftInMillis() / (1000 * 60)) % 60);
        int hours = (int) ((challenge.getTimeLeftInMillis() / (1000 * 60 * 60)) % 24);

        if (!challenge.isTimerRunning() && !challenge.isSwitchChecked()) {
            challenge.setTimeLeftInMillis(START_TIME_IN_MILLIS_EASY);
            hours = 8;
            minutes = 0;
            seconds = 0;
        } else if (!challenge.isTimerRunning() && challenge.isSwitchChecked()) {
            challenge.setTimeLeftInMillis(START_TIME_IN_MILLIS_HARD);
            hours = 24;
            minutes = 0;
            seconds = 0;
        }

        String timeLeft = String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds);
        timer.setText(timeLeft);
    }

    public String formatMilliseconds(long paMilliseconds)
    {
        int seconds = (int) (paMilliseconds / 1000) % 60;
        int minutes = (int) ((paMilliseconds / (1000 * 60)) % 60);
        int hours = (int) ((paMilliseconds / (1000 * 60 * 60)) % 24);

         String timeLeft = String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds);
         return timeLeft;
    }

    /**
     * Pomocná metóda, ktorá zabezpečuje spojenie s databázou z ktorej
     * získa potrebné informácie o dopravnej značke.
     *
     * @param paCur Cursor obsahujúci dáta o dorpavnej značke
     * @param paImageView Komponent slúžiaci pre vykreslenie náhľadu dopravnej značky.
     * @param paTextView Komponent slúžiaci pre vypísanie textu o dopravnej značke.
     */
    public void populateChallengeSignField(Cursor paCur, int paImageView, int paTextView) {
        if (paCur.moveToFirst()) {
            CharSequence name = paCur.getString(paCur.getColumnIndex("NAME"));
            byte[] image = paCur.getBlob(paCur.getColumnIndex("IMAGE"));
            Drawable drawable = ImageUtils.ByteArrayToDrawable(image);

            ImageView imageView = findViewById(paImageView);
            TextView textView = findViewById(paTextView);

            imageView.setImageDrawable(drawable);
            textView.setText(name);
        }
        paCur.close();
    }

    /**
     * Metóda zabezpečuje vykreslenie obrázkov dopravných
     * značiek a ich názvov, ktoré sú vygenerované pre
     * danú výzvu pri spustení aktivity.
     */
    public void populateChallengeSignFieldOnStart() {
        Cursor cursor1 = db.getColumn(challenge.getSign1());
        populateChallengeSignField(cursor1, R.id.imageChallenge1, R.id.textViewChallenge1);

        if (challenge.isSwitchChecked()) {
            Cursor cursor2 = db.getColumn(challenge.getSign2());
            Cursor cursor3 = db.getColumn(challenge.getSign3());

            populateChallengeSignField(cursor2, R.id.imageChallenge2, R.id.textViewChallenge2);
            populateChallengeSignField(cursor3, R.id.imageChallenge3, R.id.textViewChallenge3);
        }
    }

    public void checkResultsFailed() {
        if (!challenge.isSwitchChecked()) {
            if (!challenge.isWantedSignDone1()) {
                imgCheck1.setImageResource(R.drawable.icon_x);
            }
        } else {
            if (!challenge.isWantedSignDone1()) {
                imgCheck1.setImageResource(R.drawable.icon_x);
            }
            if (!challenge.isWantedSignDone2()) {
                imgCheck2.setImageResource(R.drawable.icon_x);
            }
            if (!challenge.isWantedSignDone3()) {
                imgCheck3.setImageResource(R.drawable.icon_x);
            }
        }
    }

    public boolean checkResultsDone() {
        if (!challenge.isSwitchChecked()) {
            if (challenge.isWantedSignDone1()) {
                return true;
            }
        } else {
            if ((challenge.isWantedSignDone1() && challenge.isWantedSignDone2()) && challenge.isWantedSignDone3()) {
                return true;
            }
        }

        return false;
    }

    public void setViews() {
        imgCheck1 = findViewById(R.id.imageChallengeCheck1);
        imgCheck2 = findViewById((R.id.imageChallengeCheck2));
        imgCheck3 = findViewById(R.id.imageChallengeCheck3);

        timer = findViewById(R.id.textViewTimer);
        Toolbar toolbar = findViewById(R.id.headerToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        statisticsButton = findViewById(R.id.buttonStatistics);

        stopButton = findViewById(R.id.buttonStopChallenge);
        startButton = findViewById(R.id.buttonStart);
        switchDiff = findViewById(R.id.switchDiff);
    }

    public void updateChallenge() {
        challengeManager.updateChallenge(challenge);
    }
}
