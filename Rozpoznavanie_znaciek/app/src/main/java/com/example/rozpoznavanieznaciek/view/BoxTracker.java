package com.example.rozpoznavanieznaciek.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Pair;
import android.util.TypedValue;


import com.example.rozpoznavanieznaciek.env.BorderedText;
import com.example.rozpoznavanieznaciek.Classifier.Recognition;
import com.example.rozpoznavanieznaciek.env.ImageUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * Trieda BoxTracker rieši „non-max suppresion“ a spája existujúce
 * objekty s novými detekciami neurónovej siete.
 */
public class BoxTracker {
    private static final float TEXT_SIZE_DIP = 20;
    private static final float MIN_SIZE = 16.0f;
    private static final int[] COLORS = {
            Color.BLUE,
            Color.RED,
            Color.GREEN,
            Color.YELLOW,
            Color.CYAN,
            Color.MAGENTA,
            Color.WHITE,
            Color.parseColor("#55FF55"),
            Color.parseColor("#FFA500"),
            Color.parseColor("#FF8888"),
            Color.parseColor("#AAAAFF"),
            Color.parseColor("#FFFFAA"),
            Color.parseColor("#55AAAA"),
            Color.parseColor("#AA33AA"),
            Color.parseColor("#0D0068")
    };
    final List<Pair<Float, RectF>> screenRects = new LinkedList<Pair<Float, RectF>>();
    private final Queue<Integer> availableColors = new LinkedList<Integer>();
    private final List<TrackedRecognition> trackedObjects = new LinkedList<TrackedRecognition>();
    private final Paint boxPaint = new Paint();
    private final float textSizePx;
    private final BorderedText borderedText;
    private Matrix frameToCanvasMatrix;
    private int frameWidth;
    private int frameHeight;
    private int sensorOrientation;
    private float left;
    private float top;
    private float right;
    private float bottom;

    private boolean multibox = true;

    public BoxTracker(final Context context) {
        for (final int color : COLORS) {
            availableColors.add(color);
        }

        boxPaint.setColor(Color.RED);
        boxPaint.setStyle(Style.STROKE);
        boxPaint.setStrokeWidth(6.0f);
        boxPaint.setStrokeCap(Cap.BUTT);
        boxPaint.setStrokeJoin(Join.MITER);
        boxPaint.setStrokeMiter(6);

        textSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, context.getResources().getDisplayMetrics());
        borderedText = new BorderedText(textSizePx);
        borderedText.setTypeface(Typeface.createFromAsset(context.getAssets(), "WorkSans-SemiBold.ttf"));
    }

    public synchronized void setFrameConfiguration(
            final int width, final int height, final int sensorOrientation) {
        frameWidth = width;
        frameHeight = height;
        this.sensorOrientation = sensorOrientation;
    }

    public synchronized void trackResults(final List<Recognition> results) {
        processResults(results);
    }

    private Matrix getFrameToCanvasMatrix() {
        return frameToCanvasMatrix;
    }

    /**
     * Synchrónna metóda, ktorá spracováva a vykresľuje rozpoznávané
     * objekty na plátno. Volá metódu triedy BorderText s parametrami
     * tak aby zabezpečila správne vykreslenie textu pri vykresľovanom plátne.
     *
     * @param canvas plátno na, ktoré sa vykresľue
     */
    public synchronized void draw(final Canvas canvas) {
        final boolean rotated = sensorOrientation % 180 == 90;
        final float multiplier =
                Math.min(
                        canvas.getHeight() / (float) (rotated ? frameWidth : frameHeight),
                        canvas.getWidth() / (float) (rotated ? frameHeight : frameWidth));

        frameToCanvasMatrix = ImageUtils.getTransformationMatrix(frameWidth, frameHeight,
                (int) (multiplier * (rotated ? frameHeight : frameWidth)),
                (int) (multiplier * (rotated ? frameWidth : frameHeight)),
                sensorOrientation,
                false);

        left = 0;
        top = 0;
        right = 0;
        bottom = 0;

        int trackResult = 0;

        for (final TrackedRecognition recognition : trackedObjects) {

            if (multibox) {
                if (trackResult == 1) {
                    continue;
                }
            }

            trackResult++;
            final RectF trackedPos = new RectF(recognition.location);

            getFrameToCanvasMatrix().mapRect(trackedPos);
            boxPaint.setColor(recognition.color);

            left = trackedPos.left;
            top = trackedPos.top;
            right = trackedPos.right;
            bottom = trackedPos.bottom;

            float cornerSize = Math.min(trackedPos.width(), trackedPos.height()) / 8.0f; //posun textu pri vykreslovaní dopravnej zančky (trackedPos.left + cornerSize)
            canvas.drawRect(trackedPos, boxPaint);

            String labelString = !TextUtils.isEmpty(recognition.title)
                    ? String.format("%s", recognition.title)
                    : String.format("%.2f", (100 * recognition.detectionConfidence));

            String[] stringField = addLinebreaks(labelString, 2);

            if (stringField[1].equals("")) {
                borderedText.drawText(canvas, trackedPos.left, trackedPos.top - borderedText.getTextSize() - 10, stringField[0], boxPaint);
            } else {
                borderedText.drawText(canvas, trackedPos.left, trackedPos.top - 2 * (borderedText.getTextSize()) - 20, stringField[0], boxPaint);
                borderedText.drawText(canvas, trackedPos.left, trackedPos.top - borderedText.getTextSize() - 10, stringField[1], boxPaint);
            }
        }
    }

    /**
     * Metóda zabezpečuje správne spracovanie rozpoznaných objektov.
     *
     * @param results List detekcií na spracovanie
     */
    private void processResults(final List<Recognition> results) {
        final List<Pair<Float, Recognition>> rectsToTrack = new LinkedList<Pair<Float, Recognition>>();

        screenRects.clear();
        final Matrix rgbFrameToScreen = new Matrix(getFrameToCanvasMatrix());

        for (final Recognition result : results) {
            if (result.getLocation() == null) {
                continue;
            }
            final RectF detectionFrameRect = new RectF(result.getLocation());

            final RectF detectionScreenRect = new RectF();
            rgbFrameToScreen.mapRect(detectionScreenRect, detectionFrameRect);


            screenRects.add(new Pair<Float, RectF>(result.getConfidence(), detectionScreenRect));

            if (detectionFrameRect.width() < MIN_SIZE || detectionFrameRect.height() < MIN_SIZE) {
                continue;
            }
            rectsToTrack.add(new Pair<Float, Recognition>(result.getConfidence(), result));
        }

        trackedObjects.clear();
        if (rectsToTrack.isEmpty()) {
            return;
        }

        for (final Pair<Float, Recognition> potential : rectsToTrack) {
            final TrackedRecognition trackedRecognition = new TrackedRecognition();
            trackedRecognition.detectionConfidence = potential.first;
            trackedRecognition.location = new RectF(potential.second.getLocation());
            trackedRecognition.title = potential.second.getTitle();
            //trackedRecognition.color = COLORS[trackedObjects.size()];
            trackedRecognition.color = Color.YELLOW;
            trackedObjects.add(trackedRecognition);

            if (trackedObjects.size() >= COLORS.length) {
                break;
            }
        }
    }

    private static class TrackedRecognition {
        RectF location;
        float detectionConfidence;
        int color;
        String title;
    }

    /**
     * Metóda slúži na zalamovanie textu pred jeho vypísaním na obrazovku.
     *
     * @param input       Vstupný text na spracovanie
     * @param maxNumWords Počet slov, za ktorými sa text zalomí
     */
    public String[] addLinebreaks(String input, int maxNumWords) {
        StringTokenizer tok = new StringTokenizer(input, " ");
        StringBuilder output = new StringBuilder(input.length());
        StringBuilder output2 = new StringBuilder(input.length());

        String[] field = new String[2];

        int words = 0;

        while (tok.hasMoreTokens()) {
            String word = tok.nextToken();

            if (words < maxNumWords) {
                output.append(word + " ");
            } else {
                output2.append(word + " ");
            }
            words++;
        }

        field[0] = output.toString();
        field[1] = output2.toString();

        return field;
    }

    public float getBottom() {
        return bottom;
    }

    public float getLeft() {
        return left;
    }

    public float getTop() { return top;}

    public float getRight() { return right; }
}
