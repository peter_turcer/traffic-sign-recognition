package com.example.rozpoznavanieznaciek;

import java.io.Serializable;

public class Challenge implements Serializable {

    private long timeLeftInMillis;
    private boolean timerRunning;

    private boolean wantedSignDone1;
    private boolean wantedSignDone2;
    private boolean wantedSignDone3;

    private int sign1;
    private int sign2;
    private int sign3;

    private boolean switchChecked;
    private boolean wasChallengeRunning;


    public Challenge() {
        timeLeftInMillis = 0;
        timerRunning = false;

        wantedSignDone1 = false;
        wantedSignDone2 = false;
        wantedSignDone3 = false;

        sign1 = 0;
        sign2 = 0;
        sign3 = 0;

        switchChecked = false;
        wasChallengeRunning = false;
    }

    public void resetAll() {
        timeLeftInMillis = 0;
        timerRunning = false;

        wantedSignDone1 = false;
        wantedSignDone2 = false;
        wantedSignDone3 = false;

        sign1 = 0;
        sign2 = 0;
        sign3 = 0;

        wasChallengeRunning = false;
    }


    public long getTimeLeftInMillis() {
        return timeLeftInMillis;
    }

    public void setTimeLeftInMillis(long timeLeftInMillis) {
        this.timeLeftInMillis = timeLeftInMillis;
    }

    public boolean isTimerRunning() {
        return timerRunning;
    }

    public void setTimerRunning(boolean timerRunning) {
        this.timerRunning = timerRunning;
    }

    public boolean isWantedSignDone1() {
        return wantedSignDone1;
    }

    public void setWantedSignDone1(boolean wantedSignDone1) {
        this.wantedSignDone1 = wantedSignDone1;
    }

    public boolean isWantedSignDone2() {
        return wantedSignDone2;
    }

    public void setWantedSignDone2(boolean wantedSignDone2) {
        this.wantedSignDone2 = wantedSignDone2;
    }

    public boolean isWantedSignDone3() {
        return wantedSignDone3;
    }

    public void setWantedSignDone3(boolean wantedSignDone3) {
        this.wantedSignDone3 = wantedSignDone3;
    }

    public int getSign1() {
        return sign1;
    }

    public void setSign1(int sign1) {
        this.sign1 = sign1;
    }


    public int getSign2() {
        return sign2;
    }

    public void setSign2(int sign2) {
        this.sign2 = sign2;
    }


    public int getSign3() {
        return sign3;
    }

    public void setSign3(int sign3) {
        this.sign3 = sign3;
    }


    public boolean isSwitchChecked() {
        return switchChecked;
    }

    public void setSwitchChecked(boolean switchChecked) {
        this.switchChecked = switchChecked;
    }

    public boolean wasChallengeRunning() {
        return wasChallengeRunning;
    }

    public void setWasChallengeRunning(boolean wasChallengeRunning) {
        this.wasChallengeRunning = wasChallengeRunning;
    }
}
