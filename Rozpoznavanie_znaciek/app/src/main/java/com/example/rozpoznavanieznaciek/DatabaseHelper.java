package com.example.rozpoznavanieznaciek;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.Nullable;

import java.util.stream.Collectors;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_PATH = "/data/data/com.example.rozpoznavanieznaciek/databases/TrafficSignDatabase.db";

    private static final String DATABASE_NAME = "TrafficSignDatabase.db";
    private static final String TABLE_NAME_SIGNS = "TrafficSigns";
    private static final String COL_1 = "ID_SIGN";
    private static final String COL_2 = "NAME";
    private static final String COL_3 = "TEXT_PRIMARY";
    private static final String COL_4 = "TEXT_INSTRUCTOR";
    private static final String COL_5 = "OPEN_STATUS";
    private static final String COL_6 = "IMAGE";

    private static final String TABLE_NAME_STAT = "Statistics";
    private static final String COL_1_STAT = "ID_STAT";
    private static final String COL_2_STAT = "DIFFICULTY";
    private static final String COL_3_STAT = "SUM_SIGNS";
    private static final String COL_4_STAT = "DURATION";


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME_SIGNS + " (ID_SIGN INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT not null, TEXT_PRIMARY TEXT not null, TEXT_INSTRUCTOR TEXT not null, OPEN_STATUS NUMERIC not null, IMAGE BLOB not null)");
        db.execSQL("create table " + TABLE_NAME_STAT + " (ID_STAT INTEGER PRIMARY KEY AUTOINCREMENT, DIFFICULTY TEXT not null, SUM_SIGNS NUMERIC not null, DURATION NUMERIC not null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SIGNS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_STAT);
        onCreate(db);
    }

    /**
     * Slúži na vkladanie riadku do databázovej tabuľky Traffic_signs
     * pomocou DML príkazu insert. Riadok alebo jeden záznam je zložený
     * z príslušných parametrov tejto metódy. Návratová hodnota metódy
     * je boolean, ktorá vráti buď true alebo false na základe úspešnosti
     * vykonaného príkazu insert.
     *
     * @param paName           Názov dopravnej značky v plnom znení
     * @param paTextPrimary    Text odpovedajúci oficiálnemu zneniu vyhlášky o príslušnej dopravnej značke.
     * @param paTextInstructor Výklad inštruktora, ktorý bližšie vysvetľuje znenie dopravnej značky.
     * @param paOpenStatus     Nadobúda hodnoty 0 alebo 1 a znázorňuje či je dopravná značka už získaná (otvorená) používateľom aplikácie alebo nie.
     * @param paImage          Obsahuje grafické vyobrazenie dopravnej značky vo formáte 300x300 pixelov
     */
    public boolean insertTrafficSign(String paName, String paTextPrimary, String paTextInstructor, int paOpenStatus, byte[] paImage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_2, paName);
        cv.put(COL_3, paTextPrimary);
        cv.put(COL_4, paTextInstructor);
        cv.put(COL_5, paOpenStatus);
        cv.put(COL_6, paImage);
        long resault = db.insert(TABLE_NAME_SIGNS, null, cv);

        if (resault == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Slúži na  vkladanie dát do databázovej tabuľky Statistics.
     * Dáta sú zložené z parametrov metódy. Metóda vráti hodnotu
     * true alebo false na základe úspešnosti vykonaného príkazu insert.
     *
     * @param paDifficulty Náročnosť vykonanej výzvy
     * @param paSumSigns   Počet získaných značiek vo výzve.
     * @param paDuration   Trvanie výzvy.
     */
    public boolean insertStatData(int paDifficulty, int paSumSigns, long paDuration) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_2_STAT, paDifficulty);
        cv.put(COL_3_STAT, paSumSigns);
        cv.put(COL_4_STAT, paDuration);

        long resault = db.insert(TABLE_NAME_STAT, null, cv);

        if (resault == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean openSign(String paName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_5, 1);

        db.update(TABLE_NAME_SIGNS, cv, COL_2 + " = ?", new String[]{paName});
        return true;
    }

    public int sumOpenTrafficSigns()
    {
        int sumSigns = 0;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME_SIGNS + " WHERE " + COL_5 + " = " + 1, null);

        if (cursor.moveToFirst()) {
            sumSigns = cursor.getInt(0);
        }
        cursor.close();

        return sumSigns;
    }


    public String getSignName(int paID) {
        String name = "";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT " + COL_2 + " FROM " + TABLE_NAME_SIGNS + " WHERE " + COL_1 + " = " + paID, null);

        if (cursor.moveToFirst()) {
            name = cursor.getString(cursor.getColumnIndex("NAME"));
        }
        cursor.close();

        return name;
    }


    public Cursor getSignByName(String paName) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_SIGNS + " WHERE " + COL_2 + " LIKE " + " ?", new String[]{paName});
        return cursor;
    }

    public Cursor getColumn(int paID) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_SIGNS + " WHERE ID_SIGN = " + paID, null);
        return cursor;
    }

    public int getTableSize(String paTableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + paTableName, null);
        return cursor.getCount();
    }

    public void deleteAllData(String paTableName) {
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "DELETE FROM " + paTableName;
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.executeUpdateDelete();
    }

    /**
     * Testovacia metóda využitá len pre developerské účely,
     * ktorá otvorí všetky zatvorené značky. V databáze Traffic_signs
     * nastaví hodnotu stĺpca OPEN_STATUS pre celú tabuľku na hodnotu 1.
     */
    public boolean openAllSigns() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_5, 1);

        db.update(TABLE_NAME_SIGNS, cv, COL_5 + " = ?", new String[]{"0"});
        return true;
    }

    public boolean resetTableSigns() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_5, 0);

        db.update(TABLE_NAME_SIGNS, cv, COL_5 + " = ?", new String[]{"1"});
        return true;
    }

    public long sumEasyDone() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME_STAT + " WHERE " + COL_2_STAT + "= 0 AND " + COL_3_STAT + " = 1", null);

        int count = 0;
        if (null != cursor)
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }

        cursor.close();
        return count;
    }

    public long sumHardDone() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME_STAT + " WHERE " + COL_2_STAT + "= 1 AND " + COL_3_STAT + " = 3", null);

        int count = 0;
        if (null != cursor)
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }

        cursor.close();
        return count;
    }

    public long sumEasyFailed() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME_STAT + " WHERE " + COL_2_STAT + "= 0 AND " + COL_3_STAT + " = 0", null);

        int count = 0;
        if (null != cursor)
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }

        cursor.close();
        return count;

    }

    public long sumHardFailed() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME_STAT + " WHERE " + COL_2_STAT + "= 1 AND " + COL_3_STAT + " BETWEEN 0 AND 2", null);

        int count = 0;
        if (null != cursor)
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }

        cursor.close();
        return count;
    }

    public long totalCollectedSigns() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUM(" + COL_3_STAT + ") FROM " + TABLE_NAME_STAT, null);

        int count = 0;
        if (null != cursor)
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }

        cursor.close();
        return count;
    }

    public long averageChallengeTime(int paDifficulty)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        long resultTime = 0 ;
        long countRows = 0;
        long sumSigns = 0;
        String difficulty = paDifficulty+"";

        Cursor cursor = db.rawQuery("SELECT SUM(" + COL_4_STAT + ") FROM " + TABLE_NAME_STAT + " WHERE " + COL_2_STAT + " = " + difficulty , null);
        Cursor cursor2 = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME_STAT + " WHERE " + COL_2_STAT + " = " + paDifficulty, null);

        if (null != cursor)
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                sumSigns = cursor.getLong(0);
            }
        cursor.close();

        if (null != cursor2)
            if (cursor2.getCount() > 0) {
                cursor2.moveToFirst();
                countRows = cursor2.getLong(0);

                if(countRows != 0)
                {
                    resultTime = sumSigns/countRows;
                }
            }
        cursor2.close();


        return resultTime;
    }

    public void recreateTableStat() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_STAT);
        db.execSQL("create table " + TABLE_NAME_STAT + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, DIFFICULTY TEXT, SUM_SIGNS NUMERIC)");
    }


    /**
     * Metóda ma za úlohu overiť či v príslušnom adresári
     * mobilnej aplikácie je už vytvorený súbor databázy.
     * Vráti hodnoty true alebo false podľa toho či nájde
     * alebo nenájde požadovaný súbor.
     */
    public boolean checkDatabase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(DATABASE_PATH, null,
                    SQLiteDatabase.OPEN_READONLY);
            checkDB.close();
        } catch (SQLiteException e) {
            System.out.println(e);
        }
        return checkDB != null;
    }
}
