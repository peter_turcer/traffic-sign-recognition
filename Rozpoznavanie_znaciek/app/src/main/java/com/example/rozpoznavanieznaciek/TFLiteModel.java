package com.example.rozpoznavanieznaciek;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.os.Trace;

import org.tensorflow.lite.Interpreter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class TFLiteModel implements Classifier {

    private static final int NUM_DETECTIONS = 10;
    private static final float IMAGE_MEAN = 128.0f;
    private static final float IMAGE_STD = 128.0f;

    private int inputSize;

    private Vector<String> labels = new Vector<String>();
    private int[] intValues;

    private float[][][] outputLocations;
    private float[][] outputClasses;
    private float[][] outputScores;
    private float[] numDetections;

    private ByteBuffer imgData;
    private Interpreter tfLite;

    private TFLiteModel() {
    }

    /**
     * Slúži na rozdelenie adresára assets ako aj inicializáciu
     * objektu modelu neurónovej siete spolu s ByteBuffrom a poľami detekcií.
     *
     * @param assetManager
     * @param modelFilename Názov modelu neurńovej siete
     * @param labelFilename Názov súboru obsahujúci označenia detekčných tried
     * @param inputSize     Predalokované buffre
     */
    public static Classifier create(final AssetManager assetManager, final String modelFilename, final String labelFilename, final int inputSize)
            throws IOException {
        final TFLiteModel tfModel = new TFLiteModel();

        InputStream labelsInput = null;
        String actualFilename = labelFilename.split("file:///android_asset/")[1];
        labelsInput = assetManager.open(actualFilename);
        BufferedReader br = null;
        br = new BufferedReader(new InputStreamReader(labelsInput));
        String line;

        while ((line = br.readLine()) != null) {
            tfModel.labels.add(line);
        }
        br.close();

        tfModel.inputSize = inputSize;

        try {
            tfModel.tfLite = new Interpreter(loadModelFile(assetManager, modelFilename));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        int numBytesPerChannel = 4;

        tfModel.imgData = ByteBuffer.allocateDirect(1 * tfModel.inputSize * tfModel.inputSize * 3 * numBytesPerChannel);
        tfModel.imgData.order(ByteOrder.nativeOrder());
        tfModel.intValues = new int[tfModel.inputSize * tfModel.inputSize];

        tfModel.outputLocations = new float[1][NUM_DETECTIONS][4];
        tfModel.outputClasses = new float[1][NUM_DETECTIONS];
        tfModel.outputScores = new float[1][NUM_DETECTIONS];
        tfModel.numDetections = new float[1];
        return tfModel;
    }


    /**
     * Metóda slúži na predpracovanie snímaných záberov,
     * spracovanie nášho modelu založeného na hodnotách s pohyblivou
     * rádovou čiarkou, skopírovanie dát do tensorov a zobrazenie najlepších
     * výsledkov detekcie
     *
     * @param bitmap Spracovávaná snímka
     */
    public List<Recognition> recognizeImage(final Bitmap bitmap) {

        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        imgData.rewind();
        for (int i = 0; i < inputSize; ++i) {
            for (int j = 0; j < inputSize; ++j) {
                int pixelValue = intValues[i * inputSize + j];

                imgData.putFloat((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat(((pixelValue & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
            }
        }

        outputLocations = new float[1][NUM_DETECTIONS][4];
        outputClasses = new float[1][NUM_DETECTIONS];
        outputScores = new float[1][NUM_DETECTIONS];
        numDetections = new float[1];

        Object[] inputArray = {imgData};
        Map<Integer, Object> outputMap = new HashMap<>();
        outputMap.put(0, outputLocations);
        outputMap.put(1, outputClasses);
        outputMap.put(2, outputScores);
        outputMap.put(3, numDetections);
        Trace.endSection();

        tfLite.runForMultipleInputsOutputs(inputArray, outputMap);

        final ArrayList<Recognition> recognitions = new ArrayList<>(NUM_DETECTIONS);

        for (int i = 0; i < NUM_DETECTIONS; ++i) {
            final RectF detection = new RectF(
                    outputLocations[0][i][1] * inputSize,
                    outputLocations[0][i][0] * inputSize,
                    outputLocations[0][i][3] * inputSize,
                    outputLocations[0][i][2] * inputSize);

            int labelOffset = 1;
            recognitions.add(new Recognition("" + i, labels.get((int) outputClasses[0][i] + labelOffset), outputScores[0][i], detection));
        }
        return recognitions;
    }


    /**
     * Metóda slúži na načítanie modelu neurńovej
     * siete vo formáte MappedByteBuffer
     *
     * @param paAssets        Asset manažér
     * @param paModelFilename Názov modelu neurńovej siete
     */
    private static MappedByteBuffer loadModelFile(AssetManager paAssets, String paModelFilename)
            throws IOException {
        AssetFileDescriptor fileDescriptor = paAssets.openFd(paModelFilename);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }
}
