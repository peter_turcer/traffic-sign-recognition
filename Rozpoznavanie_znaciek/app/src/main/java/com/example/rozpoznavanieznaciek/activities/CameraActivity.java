package com.example.rozpoznavanieznaciek.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Size;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;

import android.view.View;

import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rozpoznavanieznaciek.Challenge;
import com.example.rozpoznavanieznaciek.ChallengeManager;
import com.example.rozpoznavanieznaciek.R;
import com.example.rozpoznavanieznaciek.fragments.CameraFragment;
import com.example.rozpoznavanieznaciek.env.ImageUtils;
import com.example.rozpoznavanieznaciek.DatabaseHelper;

import org.w3c.dom.Text;

import java.util.Locale;


public abstract class CameraActivity extends AppCompatActivity implements
        Camera.PreviewCallback,
        CompoundButton.OnCheckedChangeListener,
        View.OnClickListener {
    private static final int MY_PERMISSIONS_REQUEST_USE_CAMERA = 100;

    private Runnable postInferenceCallback;
    private Runnable imageConverter;
    private int[] rgbBytes = null;

    public static final String TABLE_NAME_SIGNS = "TrafficSigns";

    private HandlerThread handlerThread;
    private Handler handler;

    private boolean isProcessingFrame = false;

    protected int previewWidth = 0;
    protected int previewHeight = 0;

    private TextView textDetection;
    private TextView timer;
    private TextView textViewSignProgress;

    private ImageView imageView;

    private ImageButton timerButton;

    private FrameLayout mainLayout;
    private RelativeLayout challengeLayout;
    private ConstraintLayout bottomLayout;

    private DatabaseHelper db;

    private Button cheatButton, resetButton, confidenceButton;

    private CountDownTimer countDownTimer;

    private Challenge challenge;
    private ChallengeManager challengeManager;

    private int exitClickCount = 0;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detection);

        findViewById(R.id.mainRelativeLayout).post(new Runnable() {
            public void run() {
                firstAppRun();
            }
        });

        db = new DatabaseHelper(this);

        if (!db.checkDatabase()) {
            populateDatabase();
        }

        challengeManager = challengeManager.getInstance(this);
        challenge = challengeManager.getChallenge();

        setViews();

        mainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                RectF rectangle = new RectF(getLeft(), getTop(), getRight(), getBottom());

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (rectangle.contains(event.getX(), event.getY())) {
                        if (challenge.isTimerRunning()) {
                            switchPreviewSignChallenge(getLabelName());
                        } else {
                            switchPreviewSign(getLabelName());
                        }
                    }
                }
                return true;
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CameraActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_USE_CAMERA);
        } else {

            Fragment fragment;
            fragment = new CameraFragment(this, getLayoutId(), getDesiredPreviewFrameSize());
            getFragmentManager().beginTransaction().replace(R.id.previewCotainer, fragment).commit();
        }


        confidenceButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                confidenceButton.setText("CF = " + changeConfidence());
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                db.resetTableSigns();
            }
        });

        cheatButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                db.openAllSigns();
            }
        });


        timerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (challengeLayout.getVisibility() == View.INVISIBLE) {
                    challengeLayout.setVisibility(View.VISIBLE);
                    timerButton.setImageResource(R.drawable.icon_stopwatch_clicked);
                } else {
                    challengeLayout.setVisibility(View.INVISIBLE);
                    timerButton.setImageResource(R.drawable.icon_stopwatch);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public void hideBottomLayout(View view) {
        bottomLayout.setVisibility(View.INVISIBLE);
    }

    public void hideTimerLayout(View view) {
        challengeLayout.setVisibility(View.INVISIBLE);
        timerButton.setImageResource(R.drawable.icon_stopwatch);
    }

    /**
     * Táto metóda zabezpečuje všetku prácu týkajúcu sa vyskakovacích okien.
     * Potrebné je zadať pohľad kde budeme dáta vypisovať a layout, ktorý tieto
     * dáta, napríklad textové polia obsahuje.
     *
     * @param view         Komponent, ktorý vykskovacie okno prekryje.
     * @param inflatedView Konkrétny komponent vyskakovacieho okna.
     */
    public void showPopupWindow(View view, int inflatedView) {

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(inflatedView, null);

        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.setFocusable(true);
        popupWindow.setTouchable(false);
        popupWindow.setOutsideTouchable(true);

        ImageButton closeButton = (ImageButton) popupView.findViewById(R.id.closeButton);

        if (inflatedView == R.layout.activity_popup_requirements) {
            TextView reqText = (TextView) popupView.findViewById(R.id.reqText2);
            reqText.setText(db.sumOpenTrafficSigns() + "/" + db.getTableSize("TrafficSigns"));
        }


        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        popupWindow.update();

        if (inflatedView != R.layout.activity_popup_tutorial) {
            popupView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    popupWindow.dismiss();
                    return true;
                }
            });
        } else
        {
            popupWindow.setFocusable(false);
            popupWindow.setTouchable(true);
            popupWindow.setOutsideTouchable(false);
            popupWindow.setBackgroundDrawable(null);

            closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });}

        popupWindow.update();
    }


    /**
     * Metóda, ktorá rozdeľuje jednotlivé komponenty menu tak, aby
     * bolo možné pri kliknutí na každý komponent vykonať inú funkcionalitu.
     *
     * @param item Položka menu.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.zoznam:
                startActivity(new Intent(this, TrafficSignListActivity.class));
                break;

            case R.id.vyzvy:

                if (checkDBopenStatus()) {
                    Intent intent = new Intent(this, DailyChallengesActivity.class);
                    saveChallengeData();
                    startActivity(intent);
                } else {
                    showPopupWindow(findViewById(R.id.mainRelativeLayout), R.layout.activity_popup_requirements);
                }
                break;

            case R.id.info:
                showPopupWindow(findViewById(R.id.mainRelativeLayout), R.layout.activity_popup_info);
                break;

            case R.id.navod:
                showPopupWindow(findViewById(R.id.mainRelativeLayout), R.layout.activity_popup_tutorial);
                break;

           /* case R.id.adminMod:
                if (cheatButton.getVisibility() == View.VISIBLE) {
                    cheatButton.setVisibility(View.INVISIBLE);
                    resetButton.setVisibility(View.INVISIBLE);
                    confidenceButton.setVisibility(View.INVISIBLE);

                } else {
                    cheatButton.setVisibility(View.VISIBLE);
                    resetButton.setVisibility(View.VISIBLE);
                    confidenceButton.setVisibility(View.VISIBLE);
                }
                break;*/
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    /**
     * Metóda vykonáva kontrolu databázy a vracia hodnoty
     * true alebo false podľa toho či používateľ už má
     * zozbierané všetky dopravné značky zo zoznamu alebo nie.
     */
    public boolean checkDBopenStatus() {
        Long opened = 0l;
        int DBsize = db.getTableSize(TABLE_NAME_SIGNS);

        for (int i = 1; i < DBsize + 1; i++) {

            Cursor cursor = db.getColumn(i);

            if (cursor.moveToFirst()) {
                opened = opened + cursor.getLong(cursor.getColumnIndex("OPEN_STATUS"));
            }
            cursor.close();
        }

        if (opened == DBsize) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_USE_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Fragment fragment;
                    fragment = new CameraFragment(this, getLayoutId(), getDesiredPreviewFrameSize());
                    getFragmentManager().beginTransaction().replace(R.id.previewCotainer, fragment).commit();
                } else {

                }
                return;
            }
        }
    }

    /**
     * Metóda, ktorá na základe mena detegovanej dopravnej značky
     * získa príslušné riadky z databázy, ktoré sú uložené v kurzore.
     * Tieto dáta posiela ďalšej metóde na spracovanie.
     *
     * @param signName Názov dopravnej značky.
     */
    public void switchPreviewSign(String signName) {

        Cursor cursor = db.getSignByName(signName);

        populateSignField(cursor);

        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
        textDetection.getLayoutParams().width = width;

        imageView.setVisibility(View.VISIBLE);
        bottomLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Táto metóda slúži na ten istý účel ako metóda switchPreviewSign()
     * s tým rozdielom, že je použitá v prípade, keď má používateľ spustenú výzvu.
     *
     * @param signName Názov dopravnej značky.
     */
    public void switchPreviewSignChallenge(String signName) {

        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
        textDetection.getLayoutParams().width = width;

        bottomLayout.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);

        String signName1 = "?";
        String signName2 = "?";
        String signName3 = "?";

        if (!challenge.isWantedSignDone1()) {
            signName1 = db.getSignName(challenge.getSign1());
        }
        if (!challenge.isWantedSignDone2()) {
            signName2 = db.getSignName(challenge.getSign2());
        }
        if (!challenge.isWantedSignDone3()) {
            signName3 = db.getSignName(challenge.getSign3());
        }

        if (signName.equals(signName1)) {
            Cursor cur1 = db.getColumn(challenge.getSign1());
            populateSignField(cur1);
            challenge.setWantedSignDone1(true);
        } else if (signName.equals(signName2)) {
            Cursor cur2 = db.getColumn(challenge.getSign2());
            populateSignField(cur2);
            challenge.setWantedSignDone2(true);
        } else if (signName.equals(signName3)) {
            Cursor cur3 = db.getColumn(challenge.getSign3());
            populateSignField(cur3);
            challenge.setWantedSignDone3(true);
        } else {
            textDetection.getLayoutParams().width = FrameLayout.LayoutParams.MATCH_PARENT;
            textDetection.requestLayout();

            imageView.setVisibility(View.INVISIBLE);
            textDetection.setText(Html.fromHtml("Táto značka už bola získaná alebo nie je z práve priebiehajúcej vývzvy"));
        }

        setNumChallengeSings();
    }

    /**
     * Metóda má za úlohu nastaviť príslušný text a náhľad dopravnej
     * značky do komponentov použitých v grafickom rozhraní aplikácie.
     *
     * @param paCur Kurzor obsahujúci dáta dopravnej značky.
     */
    public void populateSignField(Cursor paCur) {

        if (paCur.moveToFirst()) {
            String name = paCur.getString(paCur.getColumnIndex("NAME"));
            Long opened = paCur.getLong(paCur.getColumnIndex("OPEN_STATUS"));
            byte[] image = paCur.getBlob(paCur.getColumnIndex("IMAGE"));
            Drawable drawImage = ImageUtils.ByteArrayToDrawable(image);

            if (challenge.isTimerRunning()) {
                imageView.setImageDrawable(drawImage);
                textDetection.setText(Html.fromHtml("Gratulujeme, našli ste značku z výzvy:<br>" + "<b>" + name + "</b>"));
            } else if (opened == 1) {
                textDetection.setText(Html.fromHtml("Značka " + "<b>" + name + "</b> " + " už bola otvorená"));
                imageView.setImageDrawable(drawImage);
            } else {
                db.openSign(name);
                textDetection.setText(Html.fromHtml("Gratulujeme, otvorili ste značku:<br>" + "<b>" + name + "</b> " + "<br>Viac informácií si o nej môžete prečítať v sekcií \"Zoznam značiek\""));
                imageView.setImageDrawable(drawImage);
            }
        }
        paCur.close();
    }

    /**
     * Metóda spracováva obrázky snímané kamerou mobilného
     * zariadenia, ktoré konvertuje na správny formát.
     *
     * @param bytes  Pole bytov predstavujúce snímaný obrázok.
     * @param camera Komponent mobilného zariadenia.
     */
    @Override
    public void onPreviewFrame(final byte[] bytes, final Camera camera) {
        if (isProcessingFrame) {
            return;
        }

        try {
            if (rgbBytes == null) {
                Camera.Size previewSize = camera.getParameters().getPreviewSize();
                previewHeight = previewSize.height;
                previewWidth = previewSize.width;
                rgbBytes = new int[previewWidth * previewHeight];
                onPreviewSizeChosen(new Size(previewSize.width, previewSize.height), 90);
            }
        } catch (final Exception e) {
            return;
        }

        isProcessingFrame = true;

        imageConverter =
                new Runnable() {
                    @Override
                    public void run() {
                        ImageUtils.convertYUV420SPToARGB8888(bytes, previewWidth, previewHeight, rgbBytes);
                    }
                };

        postInferenceCallback =
                new Runnable() {
                    @Override
                    public void run() {
                        camera.addCallbackBuffer(bytes);
                        isProcessingFrame = false;
                    }
                };
        processImage();
    }

    @Override
    public void onBackPressed() {

        if (exitClickCount >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Pre zatvorenie aplikácie stlačte znovu tlačídlo späť.", Toast.LENGTH_SHORT).show();
            exitClickCount++;
        }
    }

    /**
     * Metóda zo životného cyklu aktivity, ktorá pri začatí aktivity
     * určí viditeľnosť komponentov a správny postup pri bežiacej výzve.
     */
    @Override
    public synchronized void onStart() {
        super.onStart();
        rgbBytes = null;

        timerButton.setVisibility(View.INVISIBLE);
        challengeLayout.setVisibility(View.INVISIBLE);
        timerButton.setImageResource(R.drawable.icon_stopwatch);
        bottomLayout.setVisibility(View.INVISIBLE);

        challengeManager = challengeManager.getInstance(this);
        challenge = challengeManager.getChallenge();

        resetImageView();


        if (challenge.isTimerRunning()) {
            if (challenge.getTimeLeftInMillis() < 0) {
                challenge.setTimeLeftInMillis(0);
                challenge.setTimerRunning(false);
                updateCountdownTimer();
            } else {
                challenge.setTimeLeftInMillis(challenge.getTimeLeftInMillis());
                setNumChallengeSings();
                startTimer();
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CameraActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_USE_CAMERA);
        } else {
            Fragment fragment;
            fragment = new CameraFragment(this, getLayoutId(), getDesiredPreviewFrameSize());
            getFragmentManager().beginTransaction().replace(R.id.previewCotainer, fragment).commit();
        }
    }

    @Override
    public synchronized void onStop() {
        super.onStop();

        saveChallengeData();

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        handlerThread = new HandlerThread("inference");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    @Override
    public synchronized void onPause() {

        handlerThread.quitSafely();
        try {
            handlerThread.join();
            handlerThread = null;
            handler = null;
        } catch (final InterruptedException e) {
        }
        super.onPause();
    }


    protected synchronized void runInBackground(final Runnable r) {
        if (handler != null) {
            handler.post(r);
        }
    }

    protected int getScreenOrientation() {
        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_270:
                return 270;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_90:
                return 90;
            default:
                return 0;
        }
    }

    protected void readyForNextImage() {
        if (postInferenceCallback != null) {
            postInferenceCallback.run();
        }
    }

    protected int[] getRgbBytes() {
        imageConverter.run();
        return rgbBytes;
    }

    /**
     * Metóda má za úlohu spravovať životný cyklus časovača pre výzvy.
     * Určuje čo sa stane po štarte časomiery, pri prejdení jednej časovej
     * jednotky a pri jej ukončení.
     */
    public void startTimer() {

        countDownTimer = new CountDownTimer(challenge.getTimeLeftInMillis(), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                challenge.setTimeLeftInMillis(millisUntilFinished);
                updateCountdownTimer();
            }

            @Override
            public void onFinish() {
                resetTimer();
                showPopupWindow(findViewById(R.id.mainRelativeLayout), R.layout.activity_popup_challenge_fail);

            }
        }.start();

        challenge.setTimerRunning(true);
        timerButton.setVisibility(View.VISIBLE);
    }

    public void resetTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        timer.setText("00:00:00");
        challenge.setTimerRunning(false);
        updateCountdownTimer();
        timerButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Zabezpečuje správny formát časomiery, formátuje ubiehajúce milisekundy
     * do tvaru hodín, minút a sekúnd.
     */
    public void updateCountdownTimer() {
        int seconds = (int) (challenge.getTimeLeftInMillis() / 1000) % 60;
        int minutes = (int) ((challenge.getTimeLeftInMillis() / (1000 * 60)) % 60);
        int hours = (int) ((challenge.getTimeLeftInMillis() / (1000 * 60 * 60)) % 24);

        String timeLeft = String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds);
        timer.setText(timeLeft);
    }

    public void setNumChallengeSings() {

        int size = 0;
        if (challenge.getSign1() != 0) {
            size++;
        }
        if (challenge.getSign2() != 0) {
            size++;
        }
        if (challenge.getSign3() != 0) {
            size++;
        }

        int done = 0;

        if (challenge.isWantedSignDone1()) {
            done++;
        }
        if (challenge.isWantedSignDone2()) {
            done++;
        }
        if (challenge.isWantedSignDone3()) {
            done++;
        }

        if ((done == size)) {
            resetTimer();
            showPopupWindow(findViewById(R.id.mainRelativeLayout), R.layout.activity_popup_challenge_done);
        }

        textViewSignProgress.setText(done + " / " + size);
    }


    public void resetImageView() {
        textDetection.setText("");
        imageView.setImageDrawable(null);
    }

    /**
     * Účelom metódy je konverzia snímok do správneho tvaru
     * a naplnenie databázy všetkými dopravnými značkami.
     */
    public void populateDatabase() {
        //db.recreateTableSigns();

        //VÝSTRAŽNÉ
        byte[] image1a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_ine_nebezepcenstvo));
        byte[] image2a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_pozor_cyklisti));
        byte[] image3a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_pozor_chodci));
        byte[] image4a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_pozor_deti));
        byte[] image5a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_pozor_priechod_pre_chodcov));
        byte[] image6a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zeleznicne_priecestie_bez_zavor));
        byte[] image7a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_obojsmerna_premavka));
        byte[] image8a = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_svetelne_signaly));

        //UPRAVUJÚCE PREDNOSŤ
        byte[] image1b = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_krizovatka_veldajsia_cesta));
        byte[] image2b = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_daj_prednost_v_jazde));
        byte[] image3b = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_stoj_daj_prednost));
        byte[] image4b = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_hlavna_cesta));

        //ZÁKAZOVÉ
        byte[] image1c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_vjazdu));
        byte[] image2c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_prechadzania));
        byte[] image3c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_nakladne));
        byte[] image4c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_vsetky_vozidla));
        byte[] image5c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_najvyssia_30));
        byte[] image6c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_najvyssia_40));
        byte[] image7c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_najvyssia_50));
        byte[] image8c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_statia));
        byte[] image9c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_zastavenia));
        byte[] image10c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_vlavo));
        byte[] image11c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_vpravo));
        byte[] image12c = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_zakaz_otacania));

        //PRÍKAZOVÉ
        byte[] image1d = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_kruhovy_objazd));
        byte[] image2d = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_cesticka_pre_cyklistov));

        //INFORMATÍVNE
        byte[] image1e = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_slepa_ulica));
        byte[] image2e = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_priechod_pre_chodcov));
        byte[] image3e = ImageUtils.DrawableToByteArray(getResources().getDrawable(R.drawable.zn_jednosmerna_cesta2));


        db.insertTrafficSign(getString(R.string.ineNebezpecenstvo_nazov), getString(R.string.ineNebezpecenstvo), getString(R.string.ineNebezpecenstvo_instruktor), 0, image1a);
        db.insertTrafficSign(getString(R.string.pozoorCyklisti_nazov), getString(R.string.pozorCyklisti), getString(R.string.pozorCyklisti_instruktor), 0, image2a);
        db.insertTrafficSign(getString(R.string.pozorChodci_nazov), getString(R.string.pozorChodci), getString(R.string.pozorChodci_instruktor), 0, image3a);
        db.insertTrafficSign(getString(R.string.pozorDeti_nazov), getString(R.string.pozorDeti), getString(R.string.pozorDeti_instruktor), 0, image4a);
        db.insertTrafficSign(getString(R.string.pozorPriechod_nazov), getString(R.string.pozorPriechod), getString(R.string.pozorPriechod_instruktor), 0, image5a);
        db.insertTrafficSign(getString(R.string.zelezPriecestie_nazov), getString(R.string.zelezPriecestie), getString(R.string.zelezPriecestie_instruktor), 0, image6a);
        db.insertTrafficSign(getString(R.string.obojsmernaPremavka_nazov), getString(R.string.obojsmernaPremavka), getString(R.string.obojsmernaPremavka_instruktor), 0, image7a);
        db.insertTrafficSign(getString(R.string.svetelneSignaly_nazov), getString(R.string.svetelneSignaly), getString(R.string.svetelneSignaly_instruktor), 0, image8a);


        db.insertTrafficSign(getString(R.string.krizovatkaVedlajsiaCesta_nazov), getString(R.string.krizovatkaVedlajsiaCesta), getString(R.string.krizovatkaVedlajsiaCesta_instruktor), 0, image1b);
        db.insertTrafficSign(getString(R.string.dajPrednost_nazov), getString(R.string.dajPrednost), getString(R.string.dajPrednost_instruktor), 0, image2b);
        db.insertTrafficSign(getString(R.string.stoj_nazov), getString(R.string.stoj), getString(R.string.stoj_instruktor), 0, image3b);
        db.insertTrafficSign(getString(R.string.hlavnaCesta_nazov), getString(R.string.hlavnaCesta), getString(R.string.hlavnaCesta_instruktor), 0, image4b);


        db.insertTrafficSign(getString(R.string.zakazVjazdu_nazov), getString(R.string.zakazVjazdu), getString(R.string.zakazVjazdu_instruktor), 0, image1c);
        db.insertTrafficSign(getString(R.string.zakazPredchadzania_nazov), getString(R.string.zakazPredchadzania), getString(R.string.zakazPredchadzania_instruktor), 0, image2c);
        db.insertTrafficSign(getString(R.string.zakazNakladne_nazov), getString(R.string.zakazNakladne), getString(R.string.zakazNakladne_instruktor), 0, image3c);
        db.insertTrafficSign(getString(R.string.zakazPremavky_nazov), getString(R.string.zakazPremavky), getString(R.string.zakazPremavky_instruktor), 0, image4c);
        db.insertTrafficSign(getString(R.string.max30_nazov), getString(R.string.max30), getString(R.string.max30_instruktor), 0, image5c);
        db.insertTrafficSign(getString(R.string.max40_nazov), getString(R.string.max40), getString(R.string.max40_instruktor), 0, image6c);
        db.insertTrafficSign(getString(R.string.max50_nazov), getString(R.string.max50), getString(R.string.max50_instruktor), 0, image7c);
        db.insertTrafficSign(getString(R.string.zakazZastavenia_nazov), getString(R.string.zakazStatia), getString(R.string.zakazStatia_instruktor), 0, image8c);
        db.insertTrafficSign(getString(R.string.zakazStatia_nazov), getString(R.string.zakazZastavenia), getString(R.string.zakazZastavenia_instruktor), 0, image9c);
        db.insertTrafficSign(getString(R.string.zakazVlavo_nazov), getString(R.string.zakazVlavo), getString(R.string.zakazVlavo_instruktor), 0, image10c);
        db.insertTrafficSign(getString(R.string.zakazVpravo_nazov), getString(R.string.zakazVpravo), getString(R.string.zakazVpravo_instruktor), 0, image11c);
        db.insertTrafficSign(getString(R.string.zakazOtacania_nazov), getString(R.string.zakazOtacania), getString(R.string.zakazOtacania_instruktor), 0, image12c);


        db.insertTrafficSign(getString(R.string.kruhovyObjazd_nazov), getString(R.string.kruhovyObjazd), getString(R.string.kruhovyObjazd_instruktor), 0, image1d);
        db.insertTrafficSign(getString(R.string.cestickaPreCyklistov_nazov), getString(R.string.cestickaPreCyklistov), getString(R.string.cestickaPreCyklistov_instruktor), 0, image2d);


        db.insertTrafficSign(getString(R.string.slepa_nazov), getString(R.string.slepa), getString(R.string.slepa_instruktor), 0, image1e);
        db.insertTrafficSign(getString(R.string.priechod_nazov), getString(R.string.priechod), getString(R.string.priechod_instruktor), 0, image2e);
        db.insertTrafficSign(getString(R.string.jednosmerka_nazov), getString(R.string.jednosmerka), getString(R.string.jednosmerka_instruktor), 0, image3e);
    }

    public void saveChallengeData() {
        challengeManager.updateChallenge(challenge);
    }

    public void setViews() {
        textDetection = (TextView) findViewById(R.id.textDetection);
        imageView = (ImageView) findViewById(R.id.imageView);
        mainLayout = (FrameLayout) findViewById(R.id.previewCotainer);
        cheatButton = (Button) findViewById(R.id.buttonCheat);
        resetButton = (Button) findViewById(R.id.buttonReset);
        confidenceButton = (Button) findViewById(R.id.buttonCF);
        timerButton = (ImageButton) findViewById(R.id.buttonShowTimer);
        bottomLayout = findViewById(R.id.recognitionPreview);
        challengeLayout = findViewById(R.id.ChallengeRelativeLayout);
        timer = (TextView) findViewById(R.id.textViewTimerMain);
        textViewSignProgress = findViewById(R.id.textViewSignProgress);
        Toolbar toolbar = findViewById(R.id.headerToolbar);

        textDetection.setText("");

        bottomLayout.setVisibility(View.INVISIBLE);
        cheatButton.setVisibility(View.INVISIBLE);
        resetButton.setVisibility(View.INVISIBLE);
        confidenceButton.setVisibility(View.INVISIBLE);

        challengeLayout.bringToFront();
        challengeLayout.setVisibility(View.INVISIBLE);
        timerButton.setVisibility(View.INVISIBLE);

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.icon_custom_menu2);
        toolbar.setOverflowIcon(drawable);
        toolbar.setTitle(getResources().getString(R.string.main_activity_name));

        setSupportActionBar(toolbar);
    }

    public void firstAppRun() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted = prefs.getBoolean("boolStart", false);
        if (!previouslyStarted) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean("boolStart", Boolean.TRUE);
            edit.apply();
            showPopupWindow(findViewById(R.id.mainRelativeLayout), R.layout.activity_popup_tutorial);
        }
    }

    protected abstract void processImage();

    protected abstract void onPreviewSizeChosen(final Size size, final int rotation);

    protected abstract int getLayoutId();

    protected abstract Size getDesiredPreviewFrameSize();

    protected abstract String getLabelName();

    protected abstract float getLeft();

    protected abstract float getRight();

    protected abstract float getBottom();

    protected abstract float getTop();

    protected abstract float changeConfidence();
}