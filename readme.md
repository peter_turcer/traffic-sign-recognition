Aplikácia apk/debug je základná aplikácia poskytujúca funkcionality pre bežného užívateľa.

Aplikácia apk/debug_admin, aplikácia s administrátorským módom poskytujúcim rôzne funkcie.
V menu aplikácie po zapnutí admin módu a použití funkcie "open DB" sa otvoria všetky dopravné značky podporované aplikáciou. 
Táto možnosť nám sprístupní novú časť aplikácie a to aktivitu výziev. 